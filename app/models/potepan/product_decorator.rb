module Potepan::ProductDecorator
  def related_products(products_count)
    Spree::Product.select("spree_products.*, count(product_id) as count").
                   in_taxons(taxons.ids).
                   where.not(id: id).
                   group("product_id").
                   order("count desc").
                   limit(products_count)
  end

  Spree::Product.prepend self
end
