module ApplicationHelper
  def full_title(page_title)
    if page_title.blank?
      Settings.title[:default]
    else
      "#{page_title} - #{Settings.title[:default]}"
    end
  end
end
