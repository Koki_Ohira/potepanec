class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(Settings.products_count[:max_count]).
                                 includes(master: [:default_price, :images])
  end
end
