require 'rails_helper'

RSpec.describe "ApplicationHelper", type: :helper do
  describe "full_title method" do
    include ApplicationHelper
    subject { full_title(page_title) }

    context "when page title is passed" do
      let(:page_title) { "Shopping" }
      it { is_expected.to eq "Shopping - BIGBAG Store" }
    end

    context "when page title is not passed" do
      let(:page_title) { "" }
      it { is_expected.to eq "BIGBAG Store" }
    end

    context "when nil is passed" do
      let(:page_title) { nil }
      it { is_expected.to eq "BIGBAG Store" }
    end
  end
end
