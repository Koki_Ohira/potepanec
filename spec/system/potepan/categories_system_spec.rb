require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  describe 'GET /potepan/categories/:id TO /potepan/product/:id' do
    let(:taxonomy1) { create(:taxonomy, name: "Category") }
    let(:taxon1) { create(:taxon, name: "Bag", parent_id: taxonomy1.root.id, taxonomy: taxonomy1) }
    let!(:taxon2) { create(:taxon, name: "Shirts", parent_id: taxonomy1.root.id, taxonomy: taxonomy1) }
    let!(:taxon3) { create(:taxon, name: "Shoes", parent_id: taxonomy1.root.id, taxonomy: taxonomy1) }
    let!(:products) { create_list(:product, 2, taxons: [taxon1]) }
    let(:product) { Spree::Product.find(products.first.id) }
    before do
      driven_by(:rack_test)
    end

    it 'display same taxons products and taxons sidebar, active products link' do
      visit potepan_category_path taxon1.id
      click_link taxonomy1.name
      within("#taxonomy_#{taxonomy1.id}") do
        expect(page).to have_selector 'li', text: taxon1.name
        expect(page).to have_selector 'li', text: taxon2.name
        expect(page).to have_selector 'li', text: taxon3.name
      end
      expect(page).to have_selector '.page-title', text: taxon1.name
      within(:css, ".product_#{products.first.id}") do
        expect(page).to have_selector 'h5', text: products.first.name
        expect(page).to have_selector 'h3', text: products.first.display_price
      end
      click_link products.first.name
      within(:css, ".media-body") do
        expect(page).to have_selector 'h2', text: product.name
        expect(page).to have_selector 'h3', text: product.display_price
        expect(page).to have_link '一覧ページへ戻る', href: "/potepan/categories/#{taxon1.id}"
        click_link '一覧ページへ戻る'
      end
      expect(page).to have_selector '.page-title', text: taxon1.name
      within(:css, ".product_#{products.second.id}") do
        expect(page).to have_selector 'h5', text: products.second.name
        expect(page).to have_selector 'h3', text: products.second.display_price
      end
    end
  end
end
