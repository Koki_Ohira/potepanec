require 'rails_helper'

RSpec.describe 'Potepan::ProductDecorator', type: :model do
  describe 'related_products(products_count)' do
    let(:products_count) { Settings.products_count[:max_count] }
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon1) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon3) { create(:taxon, taxonomy: taxonomy) }
    let!(:product1) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product2) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product3) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product4) { create(:product, taxons: [taxon2, taxon3]) }
    let!(:product5) { create(:product, taxons: [taxon2, taxon3]) }
    let!(:product6) { create(:product, taxons: [taxon2, taxon3]) }
    let(:related_products_of_product1) { product1.related_products(products_count) }
    let(:related_products_of_product4) { product4.related_products(products_count) }

    context "if the variable is product1" do
      it 'have the product2 first' do
        expect(related_products_of_product1[0]).to match product2
      end

      it 'have the product3 second' do
        expect(related_products_of_product1[1]).to match product3
      end

      it 'have the product4 third' do
        expect(related_products_of_product1[2]).to match product4
      end

      it 'have the product5 forth' do
        expect(related_products_of_product1[3]).to match product5
      end

      it 'dont have the product6 duo to the limit' do
        expect(related_products_of_product1).not_to match product6
      end
    end

    context "if the variable is product4" do
      it 'have the product5 first' do
        expect(related_products_of_product4[0]).to match product5
      end

      it 'have the product6 second' do
        expect(related_products_of_product4[1]).to match product6
      end

      it 'have the product1 third' do
        expect(related_products_of_product4[2]).to match product1
      end

      it 'have the product2 forth' do
        expect(related_products_of_product4[3]).to match product2
      end

      it 'dont have the product3 duo to the limit' do
        expect(related_products_of_product4).not_to match product3
      end
    end
  end
end
