require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/product/:id" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:products) { create_list(:product, 2, taxons: [taxon]) }
    before do
      get potepan_category_url taxon.id
    end

    it "response show action" do
      expect(response.status).to eq 200
    end

    it 'have page_title of title tag' do
      expect(response.body).to match(/<title>#{taxon.name} - BIGBAG Store<\/title>/i)
    end

    it 'display products name' do
      expect(response.body).to include "#{products.first.name}"
    end

    it 'display products price' do
      expect(response.body).to include "#{products.first.display_price}"
    end
  end
end
