require 'rails_helper'

RSpec.describe "Potepan::Sample", type: :request do
  let(:default_title) { 'BIGBAG Store' }

  describe "GET /potepan/" do
    before do
      get potepan_url
    end

    it "render show" do
      expect(response.status).to eq 200
    end

    it 'have default_title of title tag' do
      expect(response.body).to match(/<title>#{default_title}<\/title>/i)
    end
  end

  describe "GET /potepan/about_us" do
    before do
      get potepan_about_us_url
    end

    it "render show" do
      expect(response.status).to eq 200
    end

    it 'have page_title of title tag' do
      expect(response.body).to match(/<title>About us - #{default_title}<\/title>/i)
    end
  end
end
