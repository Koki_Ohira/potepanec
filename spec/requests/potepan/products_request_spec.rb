require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /potepan/product/:id" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon1) { create(:taxon) }
    let(:product) { create(:product, name: "test_product", description: "This is beautiful.", price: 29.99, taxons: [taxon1]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon1]) }
    let(:related_product1) { Spree::Product.find(related_products[0].id) }
    let(:related_product2) { Spree::Product.find(related_products[1].id) }
    let(:related_product3) { Spree::Product.find(related_products[2].id) }
    let(:related_product4) { Spree::Product.find(related_products[3].id) }
    let(:related_product5) { Spree::Product.find(related_products[4].id) }
    before do
      get potepan_product_url product.id
    end

    it "response show action" do
      expect(response.status).to eq 200
    end

    it 'have page_title of title tag' do
      expect(response.body).to match(/<title>#{product.name} - BIGBAG Store<\/title>/i)
    end

    it "have a link of products taxons path" do
      expect(response.body).to match(/<a href="\/potepan\/categories\/#{product.taxons.first.id}">/i)
    end

    it 'display products name' do
      expect(response.body).to include product.name
    end

    it 'display products description' do
      expect(response.body).to include product.description
    end

    it 'display products price' do
      expect(response.body).to include product.price.to_s
    end

    it 'display first related products name' do
      expect(response.body).to include related_product1.name
    end

    it 'display second related products name' do
      expect(response.body).to include related_product2.name
    end

    it 'display third related products name' do
      expect(response.body).to include related_product3.name
    end

    it 'display forth related products name' do
      expect(response.body).to include related_product4.name
    end

    it ' dont display fifth related products name' do
      expect(response.body).not_to include related_product5.name
    end
  end
end
