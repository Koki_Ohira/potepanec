crumb :root do
  link "Home", potepan_path
end

crumb :shop do
  link "Shop", "#"
  parent :root
end

crumb :default_title do |title|
  link "#{title.upcase}", "#"
  parent :root
end

crumb :product_title do |product|
  link "#{product.name.upcase}", potepan_product_path(product.id)
  parent :root
end

crumb :taxon_title do |taxon|
  link "#{taxon.name.upcase}", potepan_category_path(taxon.id)
  parent :shop
end
